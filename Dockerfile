FROM nimmis/apache

RUN apt-get install -y language-pack-en-base
RUN LC_ALL=en_US.UTF-8 add-apt-repository -y ppa:ondrej/php

RUN apt-get update && apt-get install -y \
apache2 \
curl \
ca-certificates \
php5.6 \
php5.6-dev \
libapache2-mod-php5.6 \
php5.6-curl \
php5.6-json \
php5.6-odbc \
php5.6-mysql \
php5.6-mcrypt \
php5.6-cli \
php5.6-gd \
php5.6-imap \
php5.6-xml \
php5.6-soap \
php5.6-mbstring \
php5.6-zip \
build-essential \
htop \
ssh \
sshpass \
npm && \
phpenmod mcrypt && a2enmod rewrite && \
phpenmod zip

# autorise .htaccess files
RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

EXPOSE 8000